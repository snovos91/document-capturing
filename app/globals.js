define([],

  function () {

    'use strict';

    var globals = {};

    globals.variables = {};

    globals.constants = {};

    globals.events = {
      BITMAP_CHANGE : 'onBitmapChanged'
    };

    globals.utils = {};

    return globals;

  });
