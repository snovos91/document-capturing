require([
  'jquery',
  'underscore',
  'backbone',
  'app',
  'router',
  'webtwain',
  'twainthumbs',
  'override',
  'bootstrap',
  'backbone.stickit'
],

function ($,_, Backbone, app,  Router, DWT, DWTThumbs) {

  'use strict';

  $(function () {
    app.router = new Router();
    app.vent = _.extend({}, Backbone.Events);
    app.counter = 0;
    app.textFlag = false;
    app.editFlag = false;
    app.isOnImage = false;
    app.selectedIndex = 0;
    app.Xcoordinate = 0;
    app.Ycoordinate = 0;
    var thumbs;
    var dwt;

    app.getDwtObject = function () {
      return dwt || (dwt = DWT.init());
    };
    app.getDwtThumbsObject = function () {
      return thumbs || (thumbs = DWTThumbs.init());
    };
    app.start();
    Backbone.history.start();
  });

});
