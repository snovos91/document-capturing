define([
  'jquery',
  'underscore',
  'backbone',
  'marionette',
  'app',
  'views/mainLayout',
  'views/headerView',
  'views/thumbnailsView',
  'views/previewView',
  'views/panelView'
],

function ($, _, Backbone, Marionette,  Application, Layout, HeaderView, ThumbnailsView, PreviewView, PanelView) {

  'use strict';

  return Application.module('MainModule', function (MainModule) {

    this.startWithParent = false;

    this.addInitializer(function () {
      this.initLayout();
      this.initViews();
      this.showRegions();
    });

    this.initLayout = function () {
      this.layout = new Layout();
      this.layout.render();
    };

    this.initViews = function () {
      this.headerView = new HeaderView();
      this.thumbsView = new ThumbnailsView();
      this.previewView = new PreviewView();
      this.panelView = new PanelView();
    };

    this.showRegions = function () {
      this.layout.gridHead.show(this.headerView);
      this.layout.gridPreviews.show(this.thumbsView);
      this.layout.gridImgPreview.show(this.previewView);
      this.layout.gridTools.show(this.panelView);
    };

  });

});


