define([
  'jquery',
  'underscore',
  'backbone',
  'marionette',
  'modules/mainModule'
],

function ($, _, Backbone, Marionette, MainModule) {

  'use strict';

  return Backbone.Router.extend({

    routes : {
      '' : 'showMain',
      'main' : 'showMain'
    },

    showMain : function () {
      MainModule.start();
    }
  });

});

