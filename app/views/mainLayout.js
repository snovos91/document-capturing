define([
  'jquery',
  'underscore',
  'backbone',
  'marionette'
],

function ($, _, Backbone, Marionette) {

  'use strict';

  return Marionette.Layout.extend({
    el : '[data-main]',
    template : 'grid-main',
    regions : {
      mainGrid : '[data-grid-container]',
      gridHead : '[data-grid-head]',
      gridPreviews : '[data-grid-previews]',
      gridImgPreview : '[data-grid-img-preview]',
      gridTools : '[data-grid-tools]'
    }
  });

});
