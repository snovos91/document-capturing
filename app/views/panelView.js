define([
  'jquery',
  'underscore',
  'backbone',
  'marionette',
  'app',
  'models/saveModel'
],

function ($, _, Backbone, Marionette, app, SaveModel) {

  'use strict';

  return Marionette.ItemView.extend({

    template : 'panel',

    events : {
      'click [data-action]' : 'actionClickHandler',
      'click [data-scan]' : 'scanClickHandler',
      'click [data-load-img]' : 'loadImgClickHandler',
      'click [data-edit-rotate-left]' : 'rotateLeftClickHandler',
      'click [data-edit-rotate-right]' : 'rotateRightClickHandler',
      'click [data-edit-text]' : 'addTextClickHandler',
      'click [data-edit-crop]' : 'cropClickHandler',
      'click .tools button' : 'bufferCheckOut',
      'click [data-local-save-btn]' : 'localSaveBtnClickHandler',
      'click [data-upload-btn]' : 'uploadBtnClickHandler'
    },

    bindings: {
      '[data-local-save]': 'saveFormat',
      '[data-server-upload]' : 'uploadFormat',
      '[data-upload-filename]' : 'fileName'
    },

    onRender : function () {
      this.saveModel = new SaveModel();
      this.stickit(this.saveModel);
    },

    scanClickHandler : function () {
      app.vent.trigger('scanDoc');
    },

    loadImgClickHandler :function () {
      app.vent.trigger('loadImg');
    },

    rotateLeftClickHandler : function () {
      app.vent.trigger('rotateLeft');
    },

    rotateRightClickHandler : function () {
      app.vent.trigger('rotateRight');
    },

    cropClickHandler : function () {
      app.vent.trigger('cropImage');
    },

    bufferCheckOut : function () {
      app.vent.trigger('checkBuffer', 'Buffer is empty');
    },

    localSaveBtnClickHandler : function () {
      app.vent.trigger('execCommand', this.saveModel.get('saveFormat'));
    },

    uploadBtnClickHandler : function () {
      app.vent.trigger('uploadImage', {
        fileName : this.saveModel.get('fileName'),
        value : this.saveModel.get('uploadFormat')
      });
    },

    addTextClickHandler : function (e) {
      e.stopPropagation();
      app.vent.trigger('addText');
    }

  });

});
