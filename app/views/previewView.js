define([
    'jquery',
    'underscore',
    'backbone',
    'marionette',
    'app',
    'views/textboxview'
],

    function ($, _, Backbone, Marionette, app, TextBoxView) {

        'use strict';

        return Marionette.ItemView.extend({

            template : 'preview',

            ui : {
                image : '[data-image-preview]',
                messageBox : '[data-message]',
                imagePreview : '[data-image-preview]',
                fakeImage : '[data-transfer-image]',
                pluginWrapper : '[data-flash-wrapper]',
                imageContainer : '[data-transfer-image-wrapper]',
                textBox : '[data-textbox]'
            },

            bindings: {
                '[data-local-save]': 'format'
            },

            events : {
                'mouseover [data-transfer-image]' : 'onMouseOnHandler',
                'mouseout [data-transfer-image]' : 'onMouseOutHandler',
                'click [data-transfer-image]' : 'addTextArea',
                'click [data-textbox]' : 'stopBubbling'
            },

            initialize : function () {
                this.listenTo(app.vent, 'execCommand', this.execCommand);
                this.listenTo(app.vent, 'scanDoc', this.scanDoc);
                this.listenTo(app.vent, 'previewImage', this.previewImage);
                this.listenTo(app.vent, 'rotateLeft', this.rotateLeft);
                this.listenTo(app.vent, 'rotateRight', this.rotateRight);
                this.listenTo(app.vent, 'cropImage', this.cropImage);
                this.listenTo(app.vent, 'checkBuffer', this.checkBuffer);
                this.listenTo(app.vent, 'showError', this.showError);
                this.listenTo(app.vent, 'uploadImage', this.uploadImage);
                this.listenTo(app.vent, 'addText', this.addText);
                this.listenTo(app.vent, 'confirmTextAdd', this.confirmTextAdd);
                this.listenTo(app.vent, 'cancelTextAdd', this.cancelTextAdd);
            },

            editFlag : false,
            textFlag : false,
            isOnImage : false,
            xcoord : 0,
            ycoord : 0,
            coordinates : {
                left : 0,
                right : 0,
                bottom : 0,
                top: 0
            },

            stopBubbling : function (e) {
                e.stopPropagation();
            },

            confirmText : function (e) {
                var self = this;
                if (self.editFlag) {
                    app.getDwtObject().done(function (dwt) {
                        self.ui.imageContainer.hide().removeClass('fake-image');
                        self.ui.pluginWrapper.show();
                        dwt.LoadDibFromClipBoard();
                        var height = dwt.GetImageHeight(0);
                        var width = dwt.GetImageWidth(0);
                        var fontSize = parseInt($('[data-textbox]').css('font-size'));
                        var font = $('[data-textbox]').css('font-family');
                        font = font.slice(0, font.indexOf(','));
                        var fontY = parseInt(fontSize * (width/600));
                        dwt.CreateTextFont(fontY, fontSize, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, font);
                        dwt.AddText(dwt.HowManyImagesInBuffer - 1, self.coordinates.left, self.coordinates.top, $('[data-textbox]').val(), 0, 16777215, 0,  1.0);
                        dwt.CopyToClipboard(0);
                        $('[data-textbox-wrapper]').parent().remove();
                        self.editFlag = false;
                        self.textFlag = false;
                        dwt.GetSelectedImagesSize(1);
                        var imagedata = dwt.SaveSelectedImagesToBase64Binary();
                        self.ui.fakeImage.attr('src', 'data:image/png;base64,' + imagedata);
                    });
                }
            },

            onMouseOnHandler : function () {
                this.isOnImage = true;
            },

            onMouseOutHandler : function () {
                this.isOnImage = false;
            },

            onClose : function () {
                console.log('canceled');
                this.ui.pluginWrapper.show();
                this.ui.imageContainer.hide();
                this.editFlag = false;
                app.getDwtObject().done(function (dwt) {
                   dwt.LoadDibFromClipBoard();
                }
                );
            },

            onRender : function () {
                this.initPreview();
                $(document.body).on('click.preview', _.bind(this.confirmText, this));
                this.ui.imageContainer.on('mouseover', _.bind(this.onMouseOnHandler, this));
                this.ui.imageContainer.on('mouseout.preview', _.bind(this.onMouseOutHandler, this));
            },

            handleSelectArea : function () {
                window.Dynamsoft_OnImageAreaSelected = _.bind(this.onSelectedArea, this);
            },

            handleDeSelectArea : function () {
                window.Dynamsoft_OnImageAreaDeselected = _.bind(this.onDeSelectedArea, this);
            },

            onDeSelectedArea : function () {
                $('[data-edit-text]').prop('disabled', true);
                $('[data-edit-crop]').prop('disabled', true);
            },

            onSelectedArea : function (sImageIndex, left, top, right, bottom){
                var self = this;
                app.getDwtObject().done(function (dwt) {
                    self.coordinates.left = left;
                    self.coordinates.right = right;
                    self.coordinates.top = top;
                    self.coordinates.bottom = bottom;
                    $('[data-edit-text]').prop('disabled',false);
                    $('[data-edit-crop]').prop('disabled', false);
                    if (self.editFlag) {
                        self.textFlag = true;
                        self.ui.pluginWrapper.hide();
                        self.ui.imageContainer.show();
                        self.createTextArea();
                    }
                });
            },



            initPreview : function () {
                var self = this;
                _.defer(function () {
                    app.getDwtObject().done(function (dwt) {
                        dwt.SetViewMode(1, 1);
                        dwt.MaxImagesInBuffer = 1;
                        dwt.VScrollBar = false;
                        dwt.EnableInteractiveZoom = true;
                        self.handleSelectArea();
                        self.handleDeSelectArea();
                    });
                });
            },

            execCommand : function (format) {
                var self = this;
                app.getDwtObject().done(function (dwt) {
                    if (dwt.HowManyImagesInBuffer < 1) {
                        self.ui.messageBox.text('No data to save');
                        return false;
                    }
                    dwt.IfShowFileDialog = true;
                    var success = dwt['SaveAs' + (format || '').toUpperCase()]('', 0);
                    if (!success) {
                        app.vent.trigger('showError', dwt.ErrorString);
                    }
                });
            },

            scanDoc : function () {
                this.Simple_AcquireImage();
            },

            previewImage : function (data) {
                var self = this;
                app.getDwtObject().done(function (dwt) {
                    if (data === -1) {
                        dwt.RemoveAllImages();
                    }
                    else {
                        dwt.SetViewMode(1, 1);
                        dwt.MaxImagesInBuffer = 1;
                        dwt.LoadDibFromClipboard();
                    }
                    dwt.GetSelectedImagesSize(1);
                    var imagedata = dwt.SaveSelectedImagesToBase64Binary();
                    var wrapRel = 600/450;
                    var imageRel = dwt.GetImageHeight(0)/dwt.GetImageWidth(0);
                    self.ui.fakeImage.attr('src', 'data:image/png;base64,' + imagedata);
                    if (imageRel > wrapRel) {
                        self.ui.fakeImage.attr('height', '100%');
                    }
                    else {
                        self.ui.fakeImage.attr('width', '100%');
                    }
                });
            },

            rotateLeft : function () {
                app.getDwtObject().done(function (dwt) {
                    dwt.RotateLeft(dwt.CurrentImageIndexInBuffer);
                });
            },

            rotateRight : function () {
                app.getDwtObject().done(function (dwt) {
                    dwt.RotateRight(dwt.CurrentImageIndexInBuffer);
                });
            },

            cropImage : function () {
                var self = this;
                app.getDwtObject().done(function (dwt) {
                    dwt.crop(dwt.HowManyImagesInBuffer - 1, self.coordinates.left, self.coordinates.top, self.coordinates.right, self.coordinates.bottom );
                    dwt.CopyToClipboard(dwt.HowManyImagesInBuffer - 1);
                    app.vent.trigger('loadCropedImage');
                });
            },

            checkBuffer : function (result) {
                var self = this;
                app.getDwtObject().done(function (dwt) {
                    if (dwt.HowManyImagesInBuffer < 1) {
                        self.ui.messageBox.text(result);
                    }
                });
            },

            uploadImage : function (data) {
                var self = this;
                app.getDwtObject().done(function (dwt) {
                    if (dwt.HowManyImagesInBuffer <1) {
                        self.ui.messageBox.text('No data to upload');
                        return false;
                    }
                    var success = dwt.HTTPUploadThroughPostEx('localhost:8001', 0, 'upload-list.asp', data.fileName, data.value );
                    if (!success) {
                        app.vent.trigger('showError', dwt.ErrorString);
                    }
                });
            },

            showError : function (error) {
                var self = this;
                app.getDwtObject().done(function (dwt) {
                    self.ui.messageBox.text(error);
                });
            },

            addText : function () {
                this.editFlag = true;
                this.ui.imageContainer.show().addClass('fake-image');
                var marginTop = parseInt(this.ui.fakeImage.height()/2);
                var marginLeft = parseInt(this.ui.fakeImage.width()/2);
                if (this.ui.fakeImage.height()%2 !== 0) {
                    marginTop -= 1;
                }
                if (this.ui.fakeImage.width()%2 !== 0) {
                    marginLeft -= 1;
                }
                this.ui.fakeImage.css({
                    marginTop : '-' + marginTop + 'px',
                    marginLeft : '-' + marginLeft + 'px'
                });
                this.createTextArea();
                this.ui.pluginWrapper.hide();
            },


            Simple_AcquireImage : function () {
                app.getDwtObject().done(function (dwt) {
                    dwt.SelectSource();
                    dwt.CloseSource();
                    dwt.OpenSource();
                    dwt.IfShowUI = false;
                    dwt.AcquireImage();
                });
            },

            createTextArea : function (e) {
                var self = this;
                app.getDwtObject().done(function (dwt) {
                    var height = dwt.GetImageHeight(0);
                    var width = dwt.GetImageWidth(0);
                    var xStep = width/450;
                    var yStep = height/600;
                    var Xcoordinate = parseInt(self.coordinates.left/xStep);
                    var Ycoordinate = parseInt(self.coordinates.top/yStep);
                    var textboxWidth = parseInt((self.coordinates.right - self.coordinates.left)/xStep);
                    var textboxHeight = parseInt((self.coordinates.bottom - self.coordinates.top)/yStep);
                    self.textBoxView = new TextBoxView();
                    self.ui.imageContainer.append(self.textBoxView.render().el);
                    self.textBoxView.$('[data-textbox-wrapper]').css({
                        left: Xcoordinate + 'px',
                        top : Ycoordinate + 'px'
                    });
                    self.textBoxView.$('[data-textbox]').css({
                        width : textboxWidth + 'px',
                        height : textboxHeight + 'px'
                    }).focus();
                });
            },

            confirmTextAdd : function () {
                var self = this;
                app.getDwtObject().done(function (dwt) {
                    self.confirmText();
                });
            },

            cancelTextAdd : function () {
                var self = this;
                app.getDwtObject().done(function (dwt) {
                    self.onClose();
                });
            }

        });

    });
