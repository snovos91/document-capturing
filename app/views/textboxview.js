define([
    'jquery',
    'underscore',
    'backbone',
    'marionette',
    'app'
],

    function ($, _, Backbone, Marionette, app) {

        'use strict';

        return Marionette.ItemView.extend({

            template : 'textbox',

            events : {
                'click [data-confirm]' : 'confirmAdd',
                'click [data-cancel-text]' : 'cancelAdd'
            },

            confirmAdd : function () {
                app.vent.trigger('confirmTextAdd');
            },

            cancelAdd : function () {
                app.vent.trigger('cancelTextAdd');
            }

        });

    });

