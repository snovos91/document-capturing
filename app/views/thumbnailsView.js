define([
  'jquery',
  'underscore',
  'backbone',
  'marionette',
  'app'
],

function ($, _, Backbone, Marionette, app) {

  'use strict';

  return Marionette.ItemView.extend({

    template : 'thumbnails',

    events : {
      'click [data-remove-all]' : 'deleteAllImages',
      'click [data-remove-selected]' : 'deleteSelectedImages'
    },

    initialize : function () {
      this.listenTo(app.vent, 'loadImg', this.loadImg);
      this.listenTo(app.vent, 'bufferSizeChanged', this.bufferSizeChanged);
      this.listenTo(app.vent, 'loadCropedImage', this.loadCropedImage);
    },

    onRender : function () {
      _.defer(_.bind(this.initThumbnails, this));
    },

    initThumbnails : function () {
      var self = this;
      app.getDwtThumbsObject().done(function (dwt) {
        dwt.SetViewMode(1, 4);
        self.handleMouseClick();
      });
    },

    handleMouseClick : function () {
      window.Thumbnail_Dynamsoft_OnMouseClick = _.bind(this.mouseClickHandler, this);
    },

    mouseClickHandler : function (index) {
      app.getDwtThumbsObject().done(function (dwt) {
        dwt.CopyToClipboard(index);
      });
      app.vent.trigger('previewImage', 1);
    },

    loadImg : function () {
      var self = this;
      app.getDwtThumbsObject().done(function (dwt) {
        dwt.IfShowFileDialog = true;
        dwt.IfShowUI = false;
        var oldCount = dwt.HowManyImagesInBuffer;
        var success = dwt.LoadImageEx('', 5);
        if (!success) {
           app.vent.trigger('showError', dwt.ErrorString);
        }
        var currentCount = dwt.HowManyImagesInBuffer;
        if (currentCount !== oldCount) {
          _(currentCount).times(function (index) {
            dwt.CopyToClipboard(index);
          });
          app.vent.trigger('previewImage', dwt.SelectedImageIndex);
        }
      });
    },

    bufferSizeChanged : function () {
      app.getDwtThumbsObject().done(function (dwt) {
        if (dwt.HowManyImagesInBuffer === 0) {
          dwt.CopyToClipboard(dwt.GetSelectedImageIndex(0));
          app.vent.trigger('previewImage', -1);
        }
        else {
          dwt.CopyToClipboard(dwt.GetSelectedImageIndex(0));
          app.vent.trigger('previewImage', 1);
        }
      });
    },



    deleteAllImages : function() {
      app.getDwtThumbsObject().done(function (dwt) {
        dwt.RemoveAllImages();
        app.vent.trigger('bufferSizeChanged');
      });
    },

    deleteSelectedImages : function() {
      app.getDwtThumbsObject().done(function (dwt) {
        dwt.RemoveAllSelectedImages();
        app.vent.trigger('bufferSizeChanged');
      });
    },

    loadCropedImage : function () {
      app.getDwtThumbsObject().done(function (dwt) {
        dwt.LoadDibFromClipboard();
      });
    }
  });

});
